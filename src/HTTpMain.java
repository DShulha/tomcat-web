import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class HTTpMain {
        public static void main(String[] args) throws IOException {
            Socket client = SSLSocketFactory.getDefault().createSocket("englishprime.ua", 443);

            OutputStreamWriter writer = new OutputStreamWriter(client.getOutputStream(), StandardCharsets.UTF_8);

            writer.append("GET /izuchenie-nepravilnyh-glagolov HTTP/1.1").append('\n');
            writer.append("Host: englishprime.ua").append('\n');

            writer.append ("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36").append('\n');
            writer.append("Accept: text/html").append('\n');
            writer.append("Connection: close").append('\n');
            writer.append("").append('\n');
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while((line = reader.readLine()) !=null){
                System.out.println(line);
            }
        }

    }


